package com.fun.p1;

import java.util.LinkedList;

public class ElementRepository {

    private LinkedList<Integer> linkedList = new LinkedList<>();

    public ElementRepository() {
        for (int i = 0; i < 10; i++) {
            linkedList.add(i);
        }
    }

    /**
     * @return find 3rd from last or -1
     */
    public int find3rdFromLast() {
        return -1;
    }
}
