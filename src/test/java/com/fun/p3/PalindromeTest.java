package com.fun.p3;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PalindromeTest {

    private Palindrome palindrome;

    @Before
    public void setup() {
        this.palindrome = new Palindrome();
    }

    @Test
    public void isPalindrome() {

        assertThat(palindrome.isPalindrome("racecar")).isTrue();

        assertThat(palindrome.isPalindrome("Redder")).isTrue();

        assertThat(palindrome.isPalindrome("aNNa")).isTrue();

        assertThat(palindrome.isPalindrome("No lemon, no melon")).isTrue();

        assertThat(palindrome.isPalindrome("I am not a palindrome")).isFalse();
    }
}
