package com.fun.p2;

public interface Queue {

    void enqueue(String element);

    String dequeue();
}
