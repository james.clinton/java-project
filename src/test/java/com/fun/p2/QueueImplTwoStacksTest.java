package com.fun.p2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class QueueImplTwoStacksTest {

    /**
     * Take next item from the queue or throw NoSuchElementException
     */
    @Test
    public void test() {
        Queue queue = null;
        queue.enqueue("a");
        queue.enqueue("b");
        queue.enqueue("c");

        final String itemA = queue.dequeue();
        assertEquals("a", itemA);
    }
}
